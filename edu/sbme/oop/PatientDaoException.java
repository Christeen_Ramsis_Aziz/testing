package edu.sbme.oop;

public class PatientDaoException extends RuntimeException {
	  PatientDaoException(Throwable e) {
		super("Patient Dao Exception", e);
	}

}

