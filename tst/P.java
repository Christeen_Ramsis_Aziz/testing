package tst;

import org.junit.Assert;
import org.junit.Test;
import edu.sbme.oop.Patient;

public class P {
	@Test
	public void constructorTest_happy() {
		Patient p =new Patient ("Ahmed","ALi") ;
		Assert.assertTrue(p.getfname().equals("Ahmed")); 
	}
	@Test
	public void falsename() {
		Patient p2=new Patient("Ahmed","");
		Assert.assertFalse(p2.getfname().equals(""));
		
	}
	@Test
	public void true_ssn() {
		Patient p3=new Patient("Ahmed","Ali");
		p3.setSSN("0128");
		Assert.assertTrue(p3.getSSN().equals("0128"));	
	} 
	@Test
	public void False_ssn() {
		Patient p3=new Patient("Ahmed","Ali");
		p3.setSSN("0128");
		Assert.assertFalse(p3.getSSN().equals("012"));	
	}
	@Test
	public void true_phn() {
		Patient p4=new Patient("Ahmed","Ali");
		p4.setPhNum("0222");
		Assert.assertTrue(p4.getPhNum().equals("0222"));	
	} 
	@Test
	public void False_phn() {
		Patient p5=new Patient("Ahmed","Ali");
		p5.setPhNum("0222");
		Assert.assertFalse(p5.getPhNum().equals("012"));	
	}
}
