package tst;

import org.junit.Assert;
import org.junit.Test;
import edu.sbme.oop.MergeUniqueArrays;
//import java.util.Arrays;

public class M {
	@Test
	public void constructorTest() {
		int[] a1=new int[]{1, 2, 3};
		int[] a2=new int[]{4};
		//MergeUniqueArrays m= new MergeUniqueArrays();
		
		//Patient p =new Patient ("Ahmed","ALi") ;
		
		
		int[] e=new int[]{1,2,3,4}; 
		int[] r=MergeUniqueArrays.mergeArrays(a1, a2);
		Assert.assertArrayEquals(e,r);
	}
}
