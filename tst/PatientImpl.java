package tst;

import org.junit.Test;

import edu.sbme.oop.Patient;
import edu.sbme.oop.PatientDaoException;
import edu.sbme.oop.PatientDaoImpl ;

public class PatientImpl {
	@Test(expected=RuntimeException.class)
	public void constructorTest() {
		PatientDaoImpl p1=new PatientDaoImpl();
		Patient pa=new Patient("Ahmed","Ali");
		pa.setPhNum("02212");
		pa.setSSN("0128");
		p1.insert_patient(pa);

  }
}
